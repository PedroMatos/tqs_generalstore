Feature: Limpar produtos do carrinho
    Como cliente
    Quero apagar todos os produtos adicionados ao meu carrinho
    Para que não os compre
Scenario: Cliente tem produtos adicionados ao seu carrinho
    Given que está na página do seu carrinho
    And que tem um ou mais produtos adicionados ao seu carrinho
    When clica no botão para remover tudo
    Then todos os produtos são retirados do seu carrinho
    