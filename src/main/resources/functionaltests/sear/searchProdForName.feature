Feature: Procurar produto por nome
    Como um cliente
    Quero procurar um produto por nome
    Para que possa escolher um produto especifico
Scenario: Existe produto
    Given Estou na página de pesquisa
    When Introduzo um nome de um produto 
    Then São apresentados os produtos com o nome igual
