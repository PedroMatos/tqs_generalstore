Feature: Addicionar produtos ao carrinho
    Como um cliente a escolher produtos
    Eu quero adicioná-los ao carrinho
    Para que os possa comprar em conjunto
Scenario: O produto é adicionado ao carrinho
    Given Estou numa página com um ou mais produtos listados
    When clico no botão de adicionar ao carrinho do produto “X”
    Then o produto “X” é adicionado ao seu carrinho


