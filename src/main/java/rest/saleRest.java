package rest;

import facades.SaleFacade;
import facades.ClientFacade;
import facades.ProductFacade;
import entities.Client;
import entities.Product;
import entities.Sale;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.PathParam;

@Path("sale")
public class saleRest {

    @EJB
    private SaleFacade facade = new SaleFacade();

    @EJB
    private ClientFacade cfacade = new ClientFacade();

    @EJB
    private ProductFacade pfacade = new ProductFacade();

    @GET
    @Path("/")
    public Response findall() {

        List<Sale> providerList = facade.findAll();
        String output = providerList.toString();
        return Response.status(200).entity(output).build();
    }

    @POST
    @Path("/{client}")
    public Response newSale(@PathParam("client") String email) {

        if (!cfacade.clientExists(email)) {
            return Response.status(204).entity("{\"State\":\"Client does not exist\"}").build();
        }
        Client desiredClient = cfacade.getClientByEmail(email);
        Sale sale = new Sale();
        sale.setClient(desiredClient);
        sale.setListProduct(new ArrayList<Product>());
        facade.create(sale);

        return Response.status(201).entity("{\"State\":\"OK\"}").build();
    }

    @POST
    @Path("/{sale}/{product}")
    public Response addProd(@PathParam("product") long prod, @PathParam("sale") long sale) {

        Product desiredProduct = pfacade.find(prod);
        if (desiredProduct == null) {
            return Response.status(204).entity("{\"State\":\"Product does not exist\"}").build();
        }
        Sale desiredSale = facade.find(sale);
        if (desiredSale == null) {
            return Response.status(204).entity("{\"State\":\"Sale does not exist\"}").build();
        }
        desiredSale.addProduct(desiredProduct);
        facade.edit(desiredSale);

        return Response.status(200).entity("{\"State\":\"OK\"}").build();
    }

    @DELETE
    @Path("/{sale}/{product}")
    public Response remProd(@PathParam("product") long prod, @PathParam("sale") long sale) {

        Product desiredProduct = pfacade.find(prod);
        if (desiredProduct == null) {
            return Response.status(204).entity("{\"State\":\"Product does not exist\"}").build();
        }
        Sale desiredSale = facade.find(sale);
        if (desiredSale == null) {
            return Response.status(204).entity("{\"State\":\"Sale does not exist\"}").build();
        }
        desiredSale.removeProd(desiredProduct);
        facade.edit(desiredSale);

        return Response.status(200).entity("{\"State\":\"OK\"}").build();
    }

    @GET
    @Path("/{id}")
    public Response find(@PathParam("id") long id) {

        Sale desiredSale = facade.find(id);
        if (desiredSale == null) {
            return Response.status(204).entity("{\"State\":\"Sale does not exist\"}").build();
        }
        String output = desiredSale.toString();
        return Response.status(200).entity(output).build();
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") long id) {

        Sale desiredSale = facade.find(id);
        if (desiredSale == null) {
            return Response.status(204).entity("{\"State\":\"Sale does not exist\"}").build();
        }
        facade.remove(desiredSale);
        return Response.status(200).entity("{\"State\":\"OK\"}").build();
    }

    @POST
    @Path("update/{id}/{client}")
    public Response edit(@PathParam("id") long id, @PathParam("client") String email) {

        if (!cfacade.clientExists(email)) {
            return Response.status(204).entity("{\"State\":\"Client does not exist\"}").build();
        }
        Client desiredClient = cfacade.getClientByEmail(email);
        Sale desiredSale = facade.find(id);
        if (desiredSale == null) {
            return Response.status(204).entity("{\"State\":\"Sale does not exist\"}").build();
        }
        desiredSale.setClient(desiredClient);
        facade.edit(desiredSale);
        
        return Response.status(200).entity("{\"State\":\"OK\"}").build();
    }
}
