package rest;

import facades.ClientFacade;
import facades.ProductFacade;
import entities.Client;
import entities.Product;
import javax.ejb.EJB;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.PathParam;

@Path("cart")
public class cartRest {

    @EJB
    private ClientFacade cfacade = new ClientFacade();

    @EJB
    private ProductFacade pfacade = new ProductFacade();

    @POST
    @Path("/{client}/{product}")
    public Response addProd(@PathParam("product") long prod, @PathParam("client") String client) {

        Product desiredProduct = pfacade.find(prod);
        if (desiredProduct == null) {
            return Response.status(204).entity("{\"State\":\"Product does not exist\"}").build();
        }
        if (!cfacade.clientExists(client)) {
            return Response.status(204).entity("{\"State\":\"Client does not exist\"}").build();
        }
        Client desiredClient = cfacade.getClientByEmail(client);
        desiredClient.getCar().addProduct(desiredProduct);
        cfacade.edit(desiredClient);

        return Response.status(201).entity("{\"State\":\"OK\"}").build();
    }

    @DELETE
    @Path("/{client}/{product}")
    public Response remProd(@PathParam("product") long prod, @PathParam("client") String client) {

        Product desiredProduct = pfacade.find(prod);
        if (desiredProduct == null) {
            return Response.status(204).entity("{\"State\":\"Product does not exist\"}").build();
        }
        if (!cfacade.clientExists(client)) {
            return Response.status(204).entity("{\"State\":\"Client does not exist\"}").build();
        }
        Client desiredClient = cfacade.getClientByEmail(client);
        desiredClient.getCar().removeProduct(desiredProduct);
        cfacade.edit(desiredClient);
        
        return Response.status(200).entity("{\"State\":\"OK\"}").build();
    }
    
    @DELETE
    @Path("/empty/{client}")
    public Response empty(@PathParam("client") String client) {

        if (!cfacade.clientExists(client)) {
            return Response.status(204).entity("{\"State\":\"Client does not exist\"}").build();
        }
        Client desiredClient = cfacade.getClientByEmail(client);
        desiredClient.getCar().clearProducts();
        cfacade.edit(desiredClient);
        
        return Response.status(200).entity("{\"State\":\"OK\"}").build();
    }
    
    @GET
    @Path("/{client}")
    public Response find(@PathParam("client") String client) {

        if (!cfacade.clientExists(client)) {
            return Response.status(204).entity("{\"State\":\"Client does not exist\"}").build();
        }
        Client desiredClient = cfacade.getClientByEmail(client);
        String output = desiredClient.getCar().toString();
        
        return Response.status(200).entity(output).build();
    }
}
