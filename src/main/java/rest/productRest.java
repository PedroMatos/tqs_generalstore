package rest;

import facades.ProductFacade;
import facades.ProviderFacade;
import entities.Product;
import entities.Provider;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.PathParam;

@Path("product")
public class productRest {

    @EJB
    private ProductFacade facade = new ProductFacade();

    @EJB
    private ProviderFacade pfacade = new ProviderFacade();

    @GET
    @Path("/")
    public Response findall() {

        List<Product> productList = facade.findAll();
        String output = productList.toString();
        return Response.status(200).entity(output).build();
    }

    @POST
    @Path("/{nome}/{price}/{description}/{url}/{category}")
    public Response add(@PathParam("nome") String name, @PathParam("price") double price,
            @PathParam("description") String description, @PathParam("url") String url,
            @PathParam("category") String category) {

        Product product = new Product();
        product.setName(name);
        product.setDescription(description);
        product.setPrice(price);
        product.setImageUrl(url);
        product.setCategory(category);
        facade.create(product);

        return Response.status(201).entity("{\"State\":\"OK\"}").build();
    }

    @GET
    @Path("/{id}")
    public Response find(@PathParam("id") long id) {

        Product desiredProduct = facade.find(id);
        if (desiredProduct == null) {
            return Response.status(204).entity("{\"State\":\"Product does not exist\"}").build();
        }
        String output = desiredProduct.toString();
        return Response.status(200).entity(output).build();
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") long id) {

        Product desiredProduct = facade.find(id);
        if (desiredProduct == null) {
            return Response.status(204).entity("{\"State\":\"Product does not exist\"}").build();
        }
        facade.remove(desiredProduct);
        return Response.status(200).entity("{\"State\":\"OK\"}").build();
    }

    @POST
    @Path("/{id}/{nome}/{price}/{description}/{url}/{category}")
    public Response edit(@PathParam("id") long id, @PathParam("nome") String name, @PathParam("price") double price,
            @PathParam("description") String description, @PathParam("url") String url,
            @PathParam("category") String category) {

        Product desiredProduct = facade.find(id);
        if (desiredProduct == null) {
            return Response.status(204).entity("{\"State\":\"Product does not exist\"}").build();
        }
        desiredProduct.setName(name);
        desiredProduct.setDescription(description);
        desiredProduct.setPrice(price);
        desiredProduct.setImageUrl(url);
        desiredProduct.setCategory(category);
        facade.edit(desiredProduct);

        return Response.status(200).entity("{\"State\":\"OK\"}").build();
    }

    @POST
    @Path("/{prod}/{provider}")
    public Response newProvider(@PathParam("provider") long id, @PathParam("prod") long prodId) {

        Provider desiredProvider = pfacade.find(id);
        if (desiredProvider == null) {
            return Response.status(204).entity("{\"State\":\"Provider does not exist\"}").build();
        }
        Product desiredProduct = facade.find(prodId);
        if (desiredProduct == null) {
            return Response.status(204).entity("{\"State\":\"Product does not exist\"}").build();
        }
        if (desiredProduct.getListProviders().contains(desiredProvider)) {
            return Response.status(204).entity("{\"State\":\"Provider is already in the list\"}").build();
        }
        desiredProduct.getListProviders().add(desiredProvider);
        facade.edit(desiredProduct);

        return Response.status(201).entity("{\"State\":\"OK\"}").build();
    }

    @DELETE
    @Path("/{prod}/{provider}")
    public Response remProvider(@PathParam("provider") long id, @PathParam("prod") long prodId) {

        Provider desiredProvider = pfacade.find(id);
        if (desiredProvider == null) {
            return Response.status(204).entity("{\"State\":\"Provider does not exist\"}").build();
        }
        Product desiredProduct = facade.find(prodId);
        if (desiredProduct == null) {
            return Response.status(204).entity("{\"State\":\"Product does not exist\"}").build();
        }
        if (!desiredProduct.getListProviders().contains(desiredProvider)) {
            return Response.status(204).entity("{\"State\":\"Provider is not in the list\"}").build();
        }
        desiredProduct.getListProviders().remove(desiredProvider);
        facade.edit(desiredProduct);

        return Response.status(200).entity("{\"State\":\"OK\"}").build();
    }

    @GET
    @Path("/providers/{prod}")
    public Response getProvider(@PathParam("prod") long prodId) {

        Product desiredProduct = facade.find(prodId);
        if (desiredProduct == null) {
            return Response.status(204).entity("{\"State\":\"Product does not exist\"}").build();
        }
        String output = desiredProduct.getListProviders().toString();

        return Response.status(200).entity(output).build();
    }
}
