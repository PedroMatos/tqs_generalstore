package rest;

import facades.ProviderFacade;
import entities.Provider;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.PathParam;

@Path("provider")
public class providerRest {

    @EJB
    private ProviderFacade facade = new ProviderFacade();

    @GET
    @Path("/")
    public Response findall() {

        List<Provider> providerList = facade.findAll();
        String output = providerList.toString();
        return Response.status(200).entity(output).build();
    }

    @POST
    @Path("/{nome}/{email}/{password}/{adress}/{ssn}")
    public Response add(@PathParam("nome") String name, @PathParam("email") String email, @PathParam("password") String password,
            @PathParam("adress") String adress, @PathParam("ssn") int ssn) {

        Provider provider = new Provider();
        provider.setName(name);
        provider.setEmail(email);
        provider.setPassword(password);
        provider.setAddress(adress);
        provider.setSSN(ssn);
        facade.create(provider);

        return Response.status(201).entity("{\"State\":\"OK\"}").build();
    }

    @GET
    @Path("/{id}")
    public Response find(@PathParam("id") long id) {

        Provider desiredProvider = facade.find(id);
        if (desiredProvider == null) {
            return Response.status(204).entity("{\"State\":\"Provider does not exist\"}").build();
        }
        String output = desiredProvider.toString();
        return Response.status(200).entity(output).build();
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") long id) {

        Provider desiredProvider = facade.find(id);
        if (desiredProvider == null) {
            return Response.status(204).entity("{\"State\":\"Provider does not exist\"}").build();
        }
        facade.remove(desiredProvider);
        return Response.status(200).entity("{\"State\":\"OK\"}").build();
    }

    @POST
    @Path("{id}/{nome}/{email}/{password}/{adress}/{ssn}")
    public Response edit(@PathParam("id") long id, @PathParam("nome") String name, @PathParam("email") String email, @PathParam("password") String password,
            @PathParam("adress") String adress, @PathParam("ssn") int ssn) {

        Provider desiredProvider = facade.find(id);
        if (desiredProvider == null) {
            return Response.status(204).entity("{\"State\":\"Provider does not exist\"}").build();
        }
        desiredProvider.setName(name);
        desiredProvider.setEmail(email);
        desiredProvider.setPassword(password);
        desiredProvider.setAddress(adress);
        desiredProvider.setSSN(ssn);
        facade.edit(desiredProvider);

        return Response.status(200).entity("{\"State\":\"OK\"}").build();
    }
}
