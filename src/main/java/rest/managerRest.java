package rest;

import facades.ManagerFacade;
import entities.Manager;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.PathParam;

@Path("manager")
public class managerRest {

    @EJB
    private ManagerFacade facade = new ManagerFacade();

    @GET
    @Path("/")
    public Response findAll() {

        List<Manager> managerList = facade.findAll();
        String output = managerList.toString();
        return Response.status(200).entity(output).build();
    }

    @POST
    @Path("/{nome}/{email}/{adress}/{password}/{SSN}")
    public Response add(@PathParam("nome") String name, @PathParam("email") String email,
            @PathParam("password") String password, @PathParam("adress") String adress,
            @PathParam("SSN") int ssn) {
        Manager manager = new Manager();
        manager.setName(name);
        manager.setEmail(email);
        manager.setPassword(password);
        manager.setAddress(adress);
        manager.setSSN(ssn);
        facade.create(manager);

        return Response.status(201).entity("{\"State\":\"OK\"}").build();
    }

    @GET
    @Path("/{id}")
    public Response find(@PathParam("id") long id) {

        Manager desiredManager = facade.find(id);
        if (desiredManager == null) {
            return Response.status(204).entity("{\"State\":\"Manager does not exist\"}").build();
        }
        String output = desiredManager.toString();
        return Response.status(200).entity(output).build();
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id")  long id) {

        Manager desiredManager = facade.find(id);
        if (desiredManager == null) {
            return Response.status(204).entity("{\"State\":\"Manager does not exist\"}").build();
        }
        facade.remove(desiredManager);
        return Response.status(200).entity("{\"State\":\"OK\"}").build();
    }

    @POST
    @Path("/{id}/{nome}/{email}/{adress}/{password}/{SSN}")
    public Response edit(@PathParam("id") long id, @PathParam("nome") String name,
            @PathParam("email") String email, @PathParam("password") String password,
            @PathParam("adress") String adress, @PathParam("SSN") int ssn) {

        Manager desiredManager = facade.find(id);
        if (desiredManager == null) {
            return Response.status(204).entity("{\"State\":\"Manager does not exist\"}").build();
        }
        desiredManager.setName(name);
        desiredManager.setEmail(email);
        desiredManager.setPassword(password);
        desiredManager.setAddress(adress);
        desiredManager.setSSN(ssn);

        facade.edit(desiredManager);

        return Response.status(200).entity("{\"State\":\"OK\"}").build();
    }
}
