package rest;

import facades.ClientFacade;
import entities.Client;
import entities.ShoppingCar;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.PathParam;

@Path("client")
public class clientRest {

    @EJB
    private ClientFacade facade = new ClientFacade();

    @GET
    @Path("/")
    public Response findAll() {

        List<Client> clientList = facade.findAll();
        String output = clientList.toString();
        return Response.status(200).entity(output).build();
    }

    @POST
    @Path("/{nome}/{email}/{adress}/{password}/{SSN}")
    public Response add(@PathParam("nome") String name, @PathParam("email") String email,
            @PathParam("password") String password, @PathParam("adress") String adress,
            @PathParam("SSN") int ssn) {
        if (!facade.validEmail(email)) {
            return Response.status(204).entity("{\"State\":\"Duplicated User\"}").build();
        }
        Client client = new Client();
        client.setName(name);
        client.setEmail(email);
        client.setPassword(password);
        client.setAddress(adress);
        client.setSSN(ssn);
        client.setCar(new ShoppingCar());
        facade.create(client);

        return Response.status(201).entity("{\"State\":\"OK\"}").build();
    }

    @GET
    @Path("/{email}")
    public Response find(@PathParam("email") String email) {

        if (!facade.clientExists(email)) {
            return Response.status(204).entity("{\"State\":\"Client does not exist\"}").build();
        }
        Client desiredClient = facade.getClientByEmail(email);
        String output = desiredClient.toString();
        return Response.status(200).entity(output).build();
    }

    @DELETE
    @Path("/{email}")
    public Response delete(@PathParam("email") String email) {

        if (!facade.clientExists(email)) {
            return Response.status(204).entity("{\"State\":\"Client does not exist\"}").build();
        }
        Client desiredClient = facade.getClientByEmail(email);
        facade.remove(desiredClient);
        return Response.status(200).entity("{\"State\":\"OK\"}").build();
    }

    @POST
    @Path("/{id}/{nome}/{email}/{adress}/{password}/{SSN}")
    public Response edit(@PathParam("id") String emailId, @PathParam("nome") String name,
            @PathParam("email") String email, @PathParam("password") String password,
            @PathParam("adress") String adress, @PathParam("SSN") int ssn) {

        if (!facade.clientExists(emailId)) {
            return Response.status(204).entity("{\"State\":\"Client does not exist\"}").build();
        }
        Client desiredClient = facade.getClientByEmail(emailId);
        desiredClient.setName(name);
        desiredClient.setEmail(email);
        desiredClient.setPassword(password);
        desiredClient.setAddress(adress);
        desiredClient.setSSN(ssn);

        facade.edit(desiredClient);

        return Response.status(200).entity("{\"State\":\"OK\"}").build();
    }
}
