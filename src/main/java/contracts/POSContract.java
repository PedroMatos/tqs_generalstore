/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package contracts;

import entities.Client;

/**
 *
 * @author matos
 */
public interface POSContract {
    public double getInterestFeeds();
    public boolean setTargetBankAcount(int NIB);
    public double makePayment(Client c, double amountToPay);
}
