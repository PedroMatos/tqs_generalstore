/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package contracts;

import entities.Sale;

/**
 *
 * @author matos
 */
public interface TransportationContract {
    public boolean makeDelivery(String supplierAddres, String destinationAddress, Sale productsToDeliver);
    public String getDeliveryStatus(Sale saleToDeliver);
    public double getInterestFees();
    public boolean payInterestFeeds(double payment);
}
