/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import entities.Client;
import entities.Provider;
import facades.ClientFacade;
import facades.ProviderFacade;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;

/**
 *
 * @author matos
 */
@Named(value = "UserView")
@SessionScoped
public class UserView implements Serializable {

    @EJB
    private ProviderFacade providerFacade = new ProviderFacade();

    @EJB
    private ClientFacade clientFacade = new ClientFacade();
    
    private String userType;
    
    /**
     * Creates a new instance of UserView
     */
    public UserView() {
    }
    public String registUser(String name, String pass, String repPass, String email,String address ,int ssn){
        if(userType.equals("Cliente")){
            if(!clientFacade.clientExists(email) && pass.equals(repPass)){
                Client c = new Client(ssn, email, pass);
                c.setName(name);
                c.setAddress(address);
                clientFacade.create(c);
            }
        }
        else{
            if(!providerFacade.providerExists(email) && pass.equals(repPass)){
                Provider p = new Provider(ssn, email, pass);
                p.setAddress(address);
                p.setSSN(ssn);
                p.setName(name);
                providerFacade.create(p);
            }
        }
            
        return "login";
    }

    public ClientFacade getClientFacade() {
        return clientFacade;
    }

    public void setClientFacade(ClientFacade clientFacade) {
        this.clientFacade = clientFacade;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }
    
    public List<Client> getAllClients(){
        return clientFacade.findAll();
    }
    
    public void deleteClient(long id){
        clientFacade.remove(clientFacade.find(id));
    }
    
    public List<Provider> getAllProviders(){
        return providerFacade.findAll();
    }
    
    public void deleteProvider(long id){
        providerFacade.remove(providerFacade.find(id));
    }
}
