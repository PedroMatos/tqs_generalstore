/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import entities.Product;
import entities.Provider;
import entities.Sale;
import facades.ProductFacade;
import facades.ProviderFacade;
import facades.SaleFacade;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
/**
 *
 * @author matos
 */
@Named(value = "ProductView")
@SessionScoped
public class ProductView implements Serializable{

    @EJB
    private ProductFacade productFacade = new ProductFacade();
    @EJB
    private ProviderFacade providerFacade = new ProviderFacade();
    @EJB
    private SaleFacade saleFacade = new SaleFacade();
    private List<Product> products;
    private Product selectedProduct;
    private String category = "Todos";
    private String productName;
    private String description;
    private String registProdCategory;
    @PostConstruct
    public void init() {
        if(category.equals("Todos"))
            products = productFacade.findAll();
        else
            getProductsByCategory(category);
    }
    public List<Product> getProducts(){
        return products;
    }
    
    public List<Product> getLoggedProviderProducts(){
        List<Product> output = new ArrayList<>();
        
        for (Product p: products)
            if (p.getListProviders().contains(providerFacade.getProviderByEmail(SessionBean.getUserName())))
                output.add(p);
        
        return output;
    }
    
    public void completeArea(String description) {
        this.description = description;
    }
    public String registProduct(String name, String price, String url){
        Product prodToRegist = new Product();
        List<Provider> l = new ArrayList<>();
        l.add(providerFacade.getProviderByEmail(SessionBean.getUserName()));
        prodToRegist.setListProviders(l);
        prodToRegist.setName(name);
        prodToRegist.setImageUrl(url);
        prodToRegist.setPrice(Double.parseDouble(price));
        prodToRegist.setDescription(description);
        prodToRegist.setCategory(registProdCategory);
        productFacade.create(prodToRegist);
        
        return "providerIndex";
    }
    
    public String editProduct(String name, String price, String url){
        Product prodToEdit = productFacade.find(selectedProduct.getProductId());
        prodToEdit.setName(name);
        prodToEdit.setImageUrl(url);
        prodToEdit.setPrice(Double.parseDouble(price));
        prodToEdit.setDescription(description);
        prodToEdit.setCategory(registProdCategory);
        productFacade.edit(prodToEdit);
        
        return "providerIndex";
    }
    
    public List<Sale> getSalesByProv(){
        
        Provider prov = providerFacade.getProviderByEmail(SessionBean.getUserName());
        System.out.println(saleFacade.findAll());
        List<Sale> allSale = saleFacade.findAll();
        List<Sale> allSalesByProv = new ArrayList<>();
        for(Sale s: allSale){
            List<Product> p = s.getListProduct();
            for (Product prod: p){
                if(prod.getListProviders().contains(prov))
                    allSalesByProv.add(s);
            }
        }
        System.out.println(allSalesByProv);
        return allSalesByProv;
    }
    
    
    public String prodsByProv(Sale s){
        Provider prov = providerFacade.getProviderByEmail(SessionBean.getUserName());
        List<Product> allProdByProv = new ArrayList<>();
        for (Product p: s.getListProduct()){
            if (p.getListProviders().contains(prov)){
                allProdByProv.add(p);
            }
        }
        
        return allProdByProv.toString();
    }
        
    public void getProductsByCategory(String category){
        products = productFacade.getProductsByCategory(category);
    }
    
    public void getProductsByName(){
        System.out.println(productName);
        products = productFacade.getProductsByName(productName);
    }
    
    public Product getSelectedProduct() {
        return selectedProduct;
    }
    
    public void removeProdFromProv(long id){
        
        Provider p = providerFacade.getProviderByEmail(SessionBean.getUserName());
        Product prod = productFacade.find(id);
        prod.rmeoveProvider(p);
        productFacade.edit(prod);
        System.out.println("removed " + p + " from prod " + id);
    }
    
    public String editProdProv(long id){
        selectedProduct = productFacade.find(id);
        
        return "editProduct";
    }

    public void setSelectedProduct(Product selectedProduct) {
        this.selectedProduct = selectedProduct;
    }
    
    public String getProviders(){
        String output = "";
        for(Provider p: selectedProduct.getListProviders())
            output += p.getName();
        
        return output;
    }

    public ProductFacade getProductFacade() {
        return productFacade;
    }

    public void setProductFacade(ProductFacade productFacade) {
        this.productFacade = productFacade;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
        getProductsByName();
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
        init();
    }

    public String getRegistProdCategory() {
        return registProdCategory;
    }

    public void setRegistProdCategory(String registProdCategory) {
        this.registProdCategory = registProdCategory;
    }
    
    public void deleteProduct(long id){
        Product p = productFacade.find(id);
        System.out.println(p);
        productFacade.remove(p);
    }
    
}
