/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import entities.Client;
import entities.Product;
import entities.Sale;
import entities.ShoppingCar;
import facades.ClientFacade;
import facades.SaleFacade;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;

/**
 *
 * @author matos
 */
@Named(value = "shoppingCarView")
@SessionScoped
public class ShoppingCarView implements Serializable {

    @EJB
    private ClientFacade clientFacade = new ClientFacade();
    @EJB
    private SaleFacade saleFacade = new SaleFacade();
    private Client cli;
    private ShoppingCar car;

    /**
     * Creates a new instance of ShoppingCarView
     */
    @PostConstruct
    public void init() {
        if (SessionBean.getUserName() != null) {
            System.out.println("entrou");
            cli = clientFacade.getClientByEmail(SessionBean.getUserName());
            car = cli.getCar();
            if (car == null)
                car = new ShoppingCar();
        } else {
            car = new ShoppingCar();
        }
    }

    public String addToCar(Product p, int amount) {
        car.addProductAmount(p, amount);
        return "listProducts";
    }

    public ShoppingCar getCar() {
        return car;
    }

    public void setCar(ShoppingCar car) {
        this.car = car;
    }

    public String completeSale() {

        Sale s = new Sale();
        Client client = clientFacade.getClientByEmail(SessionBean.getUserName());
        s.setClient(client);
        s.setListProduct(car.getProducts());
        saleFacade.create(s);
        this.car.clearProducts();
        return "index";
    }

    public double getTotalPrice(){
        double output = 0;
        for (Product p: car.getProducts()){
            output += p.getPrice();
        }
        return output;
    }
}
