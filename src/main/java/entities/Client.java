package entities;

import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class Client extends StoreUser{
    private ShoppingCar car;
    public Client(){
        super();
        car = new ShoppingCar();
    }
    public Client(int SSN, String email, String password){
        super(SSN,email,password);
        car = new ShoppingCar();
    }
    
    public ShoppingCar getCar() {
        return car;
    }

    public void setCar(ShoppingCar car) {
        this.car = car;
    }

    
    @Override
    public String toString() {
        return "{\"ClientId\":\"" + super.getUserId() + "\", \"Name\":\"" + super.getName()+ "\", \"Email\":\"" + super.getEmail() + "\","
                + " \"Address\":\"" + super.getAddress() + "\", \"SSN\":\"" + super.getSSN() + "\"}";
    }   
}
