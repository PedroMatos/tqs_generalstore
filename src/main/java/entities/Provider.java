package entities;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class Provider extends StoreUser{
    @ManyToMany(fetch=FetchType.EAGER)
    private List<Product> prods;
    public Provider(){
        super();
    }
    public Provider(int SSN, String email, String password){
        super(SSN, email, password);
        
        prods = new ArrayList<>();
    }
    public void addProduct(Product p){
        prods.add(p);
    }

    public void removeProduct(Product p){
        prods.remove(p);
    }
    
    @Override
    public String toString() {
        return "{\"ProviderId\":\"" + super.getUserId() + "\", \"Name\":\"" + super.getName() + "\", \"Address\":\"" 
                + super.getAddress() + "\", \"SSN\":\"" + super.getSSN() + "\"}";
    }
}