/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package externalentities;

import contracts.TransportationContract;
import entities.Sale;

/**
 *
 * @author matos
 */
public class Transporter {
    private TransportationContract transportCompany;

    public void setTransportCompany(TransportationContract transportCompany) {
        this.transportCompany = transportCompany;
    }
    
    
    public boolean makeDelivery(String supplierAddres, String destinationAddress, Sale productsToDeliver){
        return transportCompany.makeDelivery(supplierAddres, destinationAddress, productsToDeliver);
    }
    
    public String getDeliveryStatus(Sale saleToDeliver){
        return transportCompany.getDeliveryStatus(saleToDeliver);
    }
    
    public double getInterestFees(){
        return transportCompany.getInterestFees();
    }
    public boolean payInterestFeeds(double payment){
        return transportCompany.payInterestFeeds(payment);
    }
    
}
