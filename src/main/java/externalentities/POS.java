/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package externalentities;

import contracts.POSContract;
import entities.Client;

/**
 *
 * @author matos
 */
public class POS {
    private POSContract pos;

    public void setPos(POSContract pos) {
        this.pos = pos;
    }
    
    public double getInterestFeeds(){
        return pos.getInterestFeeds();
    }
    
    public boolean setTargetBankAcount(int NIB){
        return pos.setTargetBankAcount(NIB);
    }
    
    public double makePayment(Client c, double amountToPay){
        return pos.makePayment(c, amountToPay);
    }
}
