/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import entities.StoreUser;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author matos
 */
@Stateless
public class StoreUserFacade extends AbstractFacade<StoreUser> {

    @PersistenceContext(unitName = "todos")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public StoreUserFacade() {
        super(StoreUser.class);
    }
    
}
