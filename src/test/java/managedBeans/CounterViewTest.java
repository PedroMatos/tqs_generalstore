/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author matos
 */
public class CounterViewTest {
    private CounterView instance;
    public CounterViewTest() {
    }
    
    @Before
    public void setUp() {
        instance = new CounterView();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getNumber method, of class CounterView.
     */
    @Test
    public void testGetNumber() {
        int result = instance.getNumber();
        assertEquals(0, result);
        
    }

    /**
     * Test of increment method, of class CounterView.
     */
    @Test
    public void testIncrement() {
        instance.increment();
        assertEquals(1, instance.getNumber());
    }

    /**
     * Test of decrement method, of class CounterView.
     */
    @Test
    public void testDecrement() {
        instance.decrement();
        
        assertEquals(0, instance.getNumber());
        
        instance.increment();
        instance.decrement();
        
        assertEquals(0, instance.getNumber());
    }
    
}
