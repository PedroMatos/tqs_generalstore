/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unittests.testexternalentities;

import contracts.TransportationContract;
import entities.Client;
import entities.Product;
import entities.Sale;
import externalentities.Transporter;
import org.easymock.EasyMock;
import org.easymock.EasyMockRunner;
import org.easymock.Mock;
import org.easymock.TestSubject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;

/**
 *
 * @author matos
 */
@RunWith(EasyMockRunner.class)
public class testTransporter {
    
    public testTransporter() {
    }
    
    @TestSubject
    Transporter transporter = new Transporter();
    
    @Mock
    TransportationContract transporterMock;
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testMakeDelivery(){
        Sale testSale = new Sale(new Client());
        EasyMock.expect(transporterMock.makeDelivery("supAdress", "destAddress",
                testSale)).andReturn(Boolean.TRUE);
        //activate the mock
        EasyMock.replay(transporterMock);
        
        
        assertTrue(transporter.makeDelivery("supAdress", "destAddress", testSale));
        
        EasyMock.verify(transporterMock);
    }
    
    @Test
    public void testMakeDeliveryToInvalidSale(){
        Sale testSale = new Sale(new Client());
        EasyMock.expect(transporterMock.makeDelivery("supAdress", "destAddress",
                testSale)).andReturn(Boolean.FALSE);
        //activate the mock
        EasyMock.replay(transporterMock);
        
        
        assertFalse(transporter.makeDelivery("supAdress", "destAddress", testSale));
        
        EasyMock.verify(transporterMock);
    }
    
    @Test
    public void testPayInterestFees(){
        Sale saleForTest = new Sale(new Client());
        Product prod = new Product();
        prod.setPrice(10);
        saleForTest.addProduct(prod);
        EasyMock.expect(transporterMock.payInterestFeeds(0)).andReturn(Boolean.FALSE);
        EasyMock.expect(transporterMock.getInterestFees()).andReturn(10.0);
        EasyMock.expect(transporterMock.payInterestFeeds(saleForTest.getTotalPayedAmount()*(0.10))).andReturn(Boolean.TRUE);
        
        //activate the mock
        EasyMock.replay(transporterMock);
        
        
        assertFalse(transporter.payInterestFeeds(0));
        assertTrue(transporter.payInterestFeeds(saleForTest.getTotalPayedAmount() * (transporter.getInterestFees()/100)));
        
        EasyMock.verify(transporterMock);
    }
    
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
