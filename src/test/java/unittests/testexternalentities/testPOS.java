/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unittests.testexternalentities;

import contracts.POSContract;
import entities.Client;
import externalentities.POS;
import org.easymock.EasyMock;
import org.easymock.EasyMockRunner;
import org.easymock.Mock;
import org.easymock.TestSubject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;

/**
 *
 * @author matos
 */
@RunWith(EasyMockRunner.class)
public class testPOS {
    
    public testPOS(){
    }
    @Mock
    POSContract POSMock;
    
    @TestSubject
    POS pos = new POS();
    
    @Before
    public void setUp() {
        EasyMock.expect(POSMock.getInterestFeeds()).andReturn(10.0);
        EasyMock.expect(POSMock.makePayment(new Client(), 100)).andReturn(100.0*(10.0/100.0));
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testMakePayment(){
        EasyMock.replay(POSMock);
        assertEquals(100.0*(pos.getInterestFeeds()/100), pos.makePayment(new Client(), 100),0);
        EasyMock.verify(POSMock);
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
