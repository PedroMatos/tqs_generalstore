/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionaltests;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 *
 * @author matos
 */
public class AddProdToCartSteps {
    private WebDriver driver;
    private ShoppingCartPage cart;
    @Before
    public void prepare() {
        
    }
    @After
    public void cleanUp() {
        driver.close();
    }
    
    @Given("^Estou numa página com um ou mais produtos listados$")
    public void estou_numa_página_com_um_ou_mais_produtos_listados() throws Throwable {
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        cart = new ShoppingCartPage(driver);
    }

    @When("^clico no botão de adicionar ao carrinho do produto$")
    public void clico_no_botão_de_adicionar_ao_carrinho_do_produto_X() throws Throwable {
        cart.addToCart();
    }

    @Then("^o produto é adicionado ao seu carrinho$")
    public void o_produto_X_é_adicionado_ao_seu_carrinho() throws Throwable {
        Assert.assertEquals("Cacho Freco", cart.getProdAddedToCart());
        driver.close();
    }

}
