/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionaltests;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author matos
 */
public class ListProdPage {
    private WebDriver driver;
    private String searchedProdName;
    public ListProdPage(WebDriver driver) {
        this.driver = driver;
        this.driver.get("http://localhost:8080/tqs_generalstore/faces/listProducts.xhtml");
    }
    
    public void searchProd(String prodName){
        driver.findElement(By.id("sticky:prodname")).clear();
        driver.findElement(By.id("sticky:prodname")).sendKeys(prodName); //"nokia 3310"
        driver.findElement(By.id("sticky:j_idt14")).click();
        searchedProdName= driver.findElement(By.cssSelector("span.ui-panel-title")).getText();
    }

    public String getSearchedProdName() {
        return searchedProdName;
    }

    public void setSearchedProdName(String searchedProdName) {
        this.searchedProdName = searchedProdName;
    }
    
}
