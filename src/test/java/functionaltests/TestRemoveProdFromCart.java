package functionaltests;

import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestRemoveProdFromCart {

    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        driver = new ChromeDriver();
        baseUrl = "http://deti-tqs-vm2.ua.pt:8080/tqs_generalstore/faces/index.xhtml";
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
    }

    //@Test
    public void testRemoveProdFromCart() throws Exception {
        driver.get(baseUrl);
        driver.findElement(By.id("sticky:j_idt27")).click();
        driver.findElement(By.id("loginForm:username")).clear();
        driver.findElement(By.id("loginForm:username")).sendKeys("gustavo@ua.pt");
        driver.findElement(By.id("loginForm:password")).clear();
        driver.findElement(By.id("loginForm:password")).sendKeys("123");
        driver.findElement(By.id("loginForm:j_idt24")).click();
        driver.get("http://deti-tqs-vm2.ua.pt:8080/tqs_generalstore/faces/listProducts.xhtml");
        driver.findElement(By.xpath("//a[@id='prodgrid:0:j_idt38']/span")).click();
        driver.findElement(By.id("j_idt24:j_idt27")).click();
        driver.findElement(By.id("j_idt24:j_idt30")).click();
        driver.findElement(By.id("sticky:j_idt18")).click();
        // ERROR: Caught exception [ERROR: Unsupported command [waitForPopUp | _self | 30000]]
        driver.findElement(By.id("j_idt18:basicDT:0:j_idt27")).click();
        driver.get("http://deti-tqs-vm2.ua.pt:8080/tqs_generalstore/faces/shoppingCart.xhtml");
        assertEquals("No records found.", driver.findElement(By.cssSelector("td")).getText());
        driver.findElement(By.id("sticky:j_idt16")).click();
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
