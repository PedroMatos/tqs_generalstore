/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionaltests;

import static org.junit.Assert.assertEquals;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author matos
 */
public class ShoppingCartPage {

    private WebDriver driver;
    private String searchedProdName;
    private String baseURL;
    private String prodAddedToCart;
    private String noRecordsFounds;
    private StringBuffer verificationErrors = new StringBuffer();

    public ShoppingCartPage(WebDriver driver) {
        this.driver = driver;
        this.baseURL = "http://localhost:8080";
    }

    public void addToCart() {
        driver.get(baseURL + "/tqs_generalstore/faces/listProducts.xhtml");
        driver.findElement(By.id("sticky:j_idt16")).click();
        driver.findElement(By.id("loginForm:username")).clear();
        driver.findElement(By.id("loginForm:username")).sendKeys("gustavo@ua.pt");
        driver.findElement(By.id("loginForm:password")).clear();
        driver.findElement(By.id("loginForm:password")).sendKeys("123");
        driver.findElement(By.id("loginForm:j_idt24")).click();
        driver.findElement(By.linkText("Catálogo")).click();
        driver.findElement(By.id("prodgrid:0:j_idt38")).click();
        driver.findElement(By.id("j_idt24:j_idt27")).click();
        driver.findElement(By.id("j_idt24:j_idt27")).click();
        driver.findElement(By.id("j_idt24:j_idt30")).click();
        driver.findElement(By.id("sticky:j_idt18")).click();
        // ERROR: Caught exception [ERROR: Unsupported command [waitForPopUp | _self | 30000]]
        prodAddedToCart = driver.findElement(By.cssSelector("td")).getText();
        driver.findElement(By.id("sticky:j_idt16")).click();
    }

    public void clearCart() {
        driver.get(baseURL + "/tqs_generalstore/faces/index.xhtml");
        driver.findElement(By.id("sticky:j_idt29")).click();
        // ERROR: Caught exception [ERROR: Unsupported command [waitForPopUp | _self | 30000]]
        driver.findElement(By.id("loginForm:username")).clear();
        driver.findElement(By.id("loginForm:username")).sendKeys("gustavo@ua.pt");
        driver.findElement(By.id("loginForm:password")).clear();
        driver.findElement(By.id("loginForm:password")).sendKeys("123");
        driver.findElement(By.id("loginForm:j_idt24")).click();
        driver.findElement(By.xpath("//div[@id='sticky:j_idt10']/ul/li[2]/a/span[2]")).click();
        driver.findElement(By.xpath("//a[@id='prodgrid:0:j_idt38']/span")).click();
        driver.findElement(By.id("j_idt24:j_idt27")).click();
        driver.findElement(By.id("j_idt24:j_idt27")).click();
        driver.findElement(By.id("j_idt24:j_idt30")).click();
        driver.findElement(By.id("sticky:j_idt18")).click();
        // ERROR: Caught exception [ERROR: Unsupported command [waitForPopUp | _self | 30000]]
        driver.findElement(By.id("j_idt18:basicDT:j_idt29")).click();
        driver.findElement(By.id("j_idt18:basicDT:j_idt32")).click();
        try {
            noRecordsFounds = driver.findElement(By.cssSelector("td")).getText();
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
        driver.findElement(By.id("sticky:j_idt16")).click();
    }

    public String getProdAddedToCart() {
        return prodAddedToCart;
    }

    public String getNoRecordsFounds() {
        return noRecordsFounds;
    }
    
}
