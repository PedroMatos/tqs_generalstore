/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionaltests;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import java.util.concurrent.TimeUnit;
import static org.junit.Assert.assertTrue;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 *
 * @author matos
 */
public class ClearCartSteps {
    private WebDriver driver;
    private ShoppingCartPage cart;
    
    @Given("^que está na página do seu carrinho$")
    public void estou_na_página_de_pesquisa() throws Throwable {
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        cart = new ShoppingCartPage(driver);
    }

    @When("^clica no botão para remover tudo$")
    public void introduzo_um_nome_de_um_produto() throws Throwable {
        cart.clearCart();
    }

    @Then("^todos os produtos são retirados do seu carrinho$")
    public void são_apresentados_os_produtos_com_o_nome_igual() throws Throwable {
        assertTrue(cart.getNoRecordsFounds().equals("No records found."));
    }

}
