/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionaltests;


import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import functionaltests.ListProdPage;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.junit.*;


import static org.junit.Assert.assertTrue;

/**
 *
 * @author matos
 */
public class SearchProdByNameSteps {
    private WebDriver driver;
    private ListProdPage proPage;
   @Before
    public void prepare() {
        
    }
    @After
    public void cleanUp() {
        driver.close();
    }
    
    @Given("^Estou na página de pesquisa$")
    public void estou_na_página_de_pesquisa() throws Throwable {
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        proPage = new ListProdPage(driver);
    }

    @When("^Introduzo um nome de um produto$")
    public void introduzo_um_nome_de_um_produto() throws Throwable {
        proPage.searchProd("nokia 3310");
    }

    @Then("^São apresentados os produtos com o nome igual$")
    public void são_apresentados_os_produtos_com_o_nome_igual() throws Throwable {
        String result = proPage.getSearchedProdName();
        assertTrue(result.equalsIgnoreCase("nokia 3310"));
        driver.close();
    }


}
